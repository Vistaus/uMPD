/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UMPD_H
#define UMPD_H

#include <QObject>
#include <QDebug>
#include <QStringList>
//#include <QQuickView>
#include <QStringList>
#include <QQmlContext>

#include "mpd/client.h"
#include "playerstate.h"
#include "playerqueue.h"
#include "playercontrol.h"
#include "playlist.h"
#include "playerdatabase.h"
#include "playerstats.h"
#include "playersearch.h"
#include "playerprofile.h"
#include "utils.h"

class Umpd: public QObject {
  Q_OBJECT
  
  Q_PROPERTY(PlayerState *status READ status WRITE setStatus NOTIFY statusChanged)
  Q_PROPERTY(PlayerControl *control READ control WRITE setControl NOTIFY controlChanged)
  Q_PROPERTY(PlayerQueue *queue READ queue WRITE setQueue NOTIFY queueChanged)
  Q_PROPERTY(PlayList *playlist READ playlist WRITE setPlaylist NOTIFY playlistChanged)
  Q_PROPERTY(PlayerDatabase *database READ database WRITE setDatabase NOTIFY databaseChanged)
  Q_PROPERTY(PlayerStats *stats READ stats WRITE setStats NOTIFY statsChanged)
  Q_PROPERTY(PlayerSearch *search READ search WRITE setSearch NOTIFY searchChanged)
  Q_PROPERTY(PlayerProfile *profile READ profile WRITE setProfile NOTIFY profileChanged)
    
public:
  Umpd();
  ~Umpd();
  
  PlayerState *status();
  void setStatus(PlayerState *status);
  
  PlayerControl *control();
  void setControl(PlayerControl *control);
  
  PlayerQueue *queue();
  void setQueue(PlayerQueue *queue);

  PlayList *playlist();
  void setPlaylist(PlayList *playlist);
  
  PlayerDatabase *database();
  void setDatabase(PlayerDatabase *database);
  
  PlayerStats *stats();
  void setStats(PlayerStats *stats);
  
  PlayerSearch *search();
  void setSearch(PlayerSearch *search);
  
  PlayerProfile *profile();
  void setProfile(PlayerProfile *profile);
    
  void getStatus();
 
signals:
  void statusChanged(PlayerState *status);
  void controlChanged(PlayerControl *control);
  void queueChanged(PlayerQueue *queue);
  void playlistChanged(PlayList *playlist);
  void databaseChanged(PlayerDatabase *database);
  void statsChanged(PlayerStats *stats);
  void searchChanged(PlayerSearch *search);
  void profileChanged(PlayerProfile *profile);
  
private slots:
  void onVolumeChanged();
  void onPlayingChanged();
  void onSongChanged();
  void onQueueChanged();
  void onRepeatChanged();
  void onShuffleChanged();
  void onSeekPosChanged();
  //void onPlaylistsChanged();
  void onConnectionEstablished(struct mpd_connection *connection);
  void onConnectionClosed();

private:
  PlayerState *m_status = nullptr;
  PlayerControl *m_control = nullptr;
  PlayerQueue *m_queue = nullptr;
  PlayList *m_playlist = nullptr;
  PlayerDatabase *m_database = nullptr;
  PlayerStats *m_stats = nullptr;
  PlayerSearch *m_search = nullptr;
  PlayerProfile *m_profile = nullptr;
};

#endif
