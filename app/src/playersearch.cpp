/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playersearch.h"

PlayerSearch::PlayerSearch(QObject *parent) : Connect(parent)
{
  auto albummodel = new PlayListModel(this);
  setAlbummodel(albummodel);
}

void PlayerSearch::setAlbummodel(PlayListModel* albummodel)
{
  m_albummodel = albummodel;
}

PlayListModel * PlayerSearch::albummodel()
{
  return m_albummodel;
}

void PlayerSearch::getAlbumList(void)
{
  qDebug() << "Abfrage der Albumliste";
  mpd_search_db_tags(m_connection, MPD_TAG_ALBUM);
  mpd_search_commit(m_connection);
  
  QStringList entryTyp;
  struct mpd_pair *pair;
  
  while (pair = mpd_recv_pair_tag(m_connection, MPD_TAG_ALBUM))
  {
    m_albumlist << pair->value;
    entryTyp << "0";
    mpd_return_pair(m_connection, pair);
  }
  
  m_albumlist.sort();
  m_albumlist.removeDuplicates();
  m_albummodel->loadSearchList(entryTyp, m_albumlist);
}

void PlayerSearch::getSongsFromAlbum(QString album)
{
  qDebug() << "Abfrage der Titel im ausgewaehlten Album:" << album;
  
  setName(album);
  
  mpd_search_db_songs(m_connection, true);
  mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, utils::QStringToBA(album).data());
  mpd_search_commit(m_connection);
  
  QStringList albumTitle;
  QStringList entryTyp;
  struct mpd_pair *pair;
  
  while (pair = mpd_recv_pair_tag(m_connection, MPD_TAG_TITLE))
  {
    albumTitle << pair->value;
    entryTyp << "1";
    mpd_return_pair(m_connection, pair);
  }
  m_albummodel->loadSearchList(entryTyp, albumTitle);
}

void PlayerSearch::forAlbum(QString album)
{
  qDebug() << "Album suchen";
  
  QStringList result;
  QStringList entryTyp;

  result = m_albumlist.filter(album,Qt::CaseInsensitive);
  
  for(QString item: result) {
    entryTyp << "0";
  }
  
  m_albummodel->loadSearchList(entryTyp, result);
}

bool PlayerSearch::add(QString album, unsigned tag)
{
  struct mpd_pair *pair;

  bool stat = mpd_search_add_db_songs(m_connection, true);
  switch(tag) {
    case 1:
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, utils::QStringToBA(album).data());
      break;
    case 2:
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ARTIST, utils::QStringToBA(album).data());
      break;
    case 3:
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, utils::QStringToBA(Name()).data());
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_TITLE, utils::QStringToBA(album).data());
      break;
    case 4:
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ARTIST, utils::QStringToBA(Name()).data());
      stat = mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_TITLE, utils::QStringToBA(album).data());
      break;
  }
  stat = mpd_search_commit(m_connection);
  
  while (pair = mpd_recv_pair_tag(m_connection, MPD_TAG_ALBUM))
  {
    mpd_return_pair(m_connection, pair);
  }
  
  return stat;
}

void PlayerSearch::getArtistList(void)
{
  qDebug() << "Abfrage der Artistliste";
  mpd_search_db_tags(m_connection, MPD_TAG_ARTIST);
  mpd_search_commit(m_connection);
  
  QStringList entryTyp;
  struct mpd_pair *pair;
  
  while (pair = mpd_recv_pair_tag(m_connection, MPD_TAG_ARTIST))
  {
    m_artistList << pair->value;
    entryTyp << "0";
    mpd_return_pair(m_connection, pair);
  }
  
  m_artistList.sort();
  m_artistList.removeDuplicates();
  m_albummodel->loadSearchList(entryTyp, m_artistList);
}

void PlayerSearch::forArtist(QString artist)
{
  qDebug() << "Artist suchen";
  
  QStringList result;
  QStringList entryTyp;

  result = m_artistList.filter(artist,Qt::CaseInsensitive);
  
  for(QString item: result) {
    entryTyp << "0";
  }
  
  m_albummodel->loadSearchList(entryTyp, result);
}

void PlayerSearch::getSongsFromArtist(QString artist)
{
  qDebug() << "Abfrage der Titel vom ausgewaehlten Artist:" << artist;
  
  setName(artist);
  
  mpd_search_db_songs(m_connection, true);
  mpd_search_add_tag_constraint(m_connection, MPD_OPERATOR_DEFAULT, MPD_TAG_ARTIST, utils::QStringToBA(artist).data());
  mpd_search_commit(m_connection);
  
  QStringList title;
  QStringList entryTyp;
  struct mpd_pair *pair;
  
  while (pair = mpd_recv_pair_tag(m_connection, MPD_TAG_TITLE))
  {
    title << pair->value;
    entryTyp << "1";
    mpd_return_pair(m_connection, pair);
  }
  m_albummodel->loadSearchList(entryTyp, title);
}

void PlayerSearch::setName(QString name)
{
  if(name != m_name) {
    m_name = name;
    emit updateName(m_name);
  }
}

QString PlayerSearch::Name() const
{
  return m_name;
}
