/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QQuickView>

#include "src/umpd.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationName("umpd.stefanweng");

    QCoreApplication::setApplicationName(QStringLiteral("umpd.stefanweng"));

    qmlRegisterType<Umpd>("Umpd", 1, 0, "Umpd");

    QQuickView *view = new QQuickView();
    view->setSource(QUrl(QStringLiteral("qrc:/Main.qml")));
    view->rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();
    
    return app.exec();
}
